# PostProcessSubdivPlugin

This Plugin allows post-process subdivision.

To perform subdivision, one has first to select the objet to be subdivided.
Then on the PostSubdivPlugin widget, one can select the desired subdivision method,
as well as the desired number of iterations.
