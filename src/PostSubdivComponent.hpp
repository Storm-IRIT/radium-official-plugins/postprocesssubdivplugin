#ifndef POSTSUBDIV_PLUGIN_POSTSUBDIV_COMPONENT_HPP_
#define POSTSUBDIV_PLUGIN_POSTSUBDIV_COMPONENT_HPP_

#include <PostSubdivPluginMacros.hpp>

#include <Core/Geometry/CatmullClarkSubdivider.hpp>
#include <Core/Geometry/LoopSubdivider.hpp>
#include <Core/Asset/HandleData.hpp>
#include <Core/Utils/Index.hpp>
#include <Core/Geometry/TriangleMesh.hpp>

#include <Engine/Component/Component.hpp>
#include <Engine/Managers/ComponentMessenger/ComponentMessenger.hpp>

namespace Ra {
namespace Engine {
class RenderObject;
class Mesh;
}
} // namespace Ra

namespace PostSubdivPlugin {

using Ra::Core::Geometry::CatmullClarkSubdivider;
using Ra::Core::Geometry::LoopSubdivider;
using Ra::Core::Geometry::TopologicalMesh;
using Ra::Core::Geometry::TriangleMesh;

/// The PostSubdivComponent class is responsible for applying post-process
/// subdivision on a mesh.
class POST_SUBDIV_PLUGIN_API PostSubdivComponent : public Ra::Engine::Component {
  public:
    /// The Subdivision Method.
    enum SubdivMethod {
        LOOP = 0,    ///< Loop subdivision
        CATMULLCLARK ///< CatmullClark subdivision
    };

    PostSubdivComponent( const std::string& name, SubdivMethod method, uint iter,
                         Ra::Engine::Entity* entity ) :
        Component( name, entity ),
        m_subdivMethod( method ),
        m_iter( iter ) {}

    virtual ~PostSubdivComponent();

    virtual void initialize() override;

    /// Set the id to get data from the ComponentMessenger.
    void setContentsName( const std::string& name ) { m_contentsName = name; }

    /// Apply the Subdivision on the mesh.
    void subdiv();

    /// Sets the subdivision method to use.
    void setSubdivMethod( SubdivMethod method );

    /// \returns the current subdivision method.
    inline SubdivMethod getSubdivMethod() const { return m_subdivMethod; }

    /// Sets the number of subdivision operations.
    void setSubdivIter( uint iter ) { m_iter = iter; }

    /// \returns the number of subdivision operations.
    uint getSubdivIter() const { return m_iter; }

  private:
    /// The Entity name for Component communication.
    std::string m_contentsName;

    /// The Subdivision Method.
    SubdivMethod m_subdivMethod;

    /// The number of subdivision iterations to perform.
    uint m_iter;

    /// Display RO.
    Ra::Engine::RenderObject* m_ro;
    std::shared_ptr<Ra::Engine::Mesh> m_mesh;

    /// Getter for the Geometry RO index.
    Ra::Engine::ComponentMessenger::CallbackTypes<Ra::Core::Utils::Index>::Getter m_roIdxGetter;

    using Subdivider = OpenMesh::Subdivider::Uniform::SubdividerT<TopologicalMesh, Scalar>;
    /// The list of per-subdiv-iteration triplets of subdivider-topoMesh-TriangleMesh.
    std::map<uint, std::tuple<Subdivider*, TopologicalMesh, TriangleMesh>> m_subdivData;
};

} // namespace PostSubdivPlugin

#endif //  POSTSUBDIV_PLUGIN_POSTSUBDIV_COMPONENT_HPP_
