#include <PostSubdivComponent.hpp>

#include <Engine/Renderer/Mesh/Mesh.hpp>
#include <Engine/Renderer/RenderObject/RenderObject.hpp>
#include <Engine/Renderer/RenderObject/RenderObjectManager.hpp>

namespace PostSubdivPlugin {

PostSubdivComponent::~PostSubdivComponent() {
    for ( auto s : m_subdivData )
    {
        std::get<0>( s.second )->detach();
        delete std::get<0>( s.second );
    }
}

void PostSubdivComponent::initialize() {
    auto compMsg = Ra::Engine::ComponentMessenger::getInstance();

    bool hasMesh = compMsg->canGet<TriangleMesh>( getEntity(), m_contentsName );
    if ( hasMesh )
    {
        m_roIdxGetter = compMsg->getterCallback<Ra::Core::Utils::Index>( getEntity(), m_contentsName );
        auto RO = getRoMgr()->getRenderObject( *( m_roIdxGetter() ) );
        // create the display RO
        m_mesh.reset( new Ra::Engine::Mesh( "PostSubdiv_Mesh" ) );
        TriangleMesh mesh = *compMsg->getterCallback<TriangleMesh>( getEntity(), m_contentsName )();
        m_mesh->loadGeometry( std::move( mesh ) );
        m_ro = new Ra::Engine::RenderObject( "PostSubdiv_RO", this,
                                             Ra::Engine::RenderObjectType::Geometry );
        m_ro->setVisible( false );
        m_ro->setMesh( m_mesh );
        m_ro->setRenderTechnique( RO->getRenderTechnique() );
        m_ro->setMaterial( RO->getMaterial() );
        addRenderObject( m_ro );
        // subdiv data will be created at first run
    }
}

void PostSubdivComponent::setSubdivMethod( SubdivMethod method ) {
    m_subdivMethod = method;
    m_subdivData.clear();
}

void PostSubdivComponent::subdiv() {
    auto RO = getRoMgr()->getRenderObject( *( m_roIdxGetter() ) );
    if ( m_iter == 0 )
    {
        RO->setVisible( true );
        m_ro->setVisible( false );
        return;
    }
    RO->setVisible( false );
    m_ro->setVisible( true );
    // get mesh to deform
    auto compMsg = Ra::Engine::ComponentMessenger::getInstance();
    const TriangleMesh& mesh = compMsg->get<TriangleMesh>( getEntity(), m_contentsName );
    // apply subdiv
    auto it = m_subdivData.find( m_iter );
    if ( it == m_subdivData.end() )
    {
        switch ( m_subdivMethod )
        {
        case LOOP:
            m_subdivData[m_iter] = std::make_tuple( new Ra::Core::Geometry::LoopSubdivider(),
                                                    TopologicalMesh( mesh ), TriangleMesh() );
            break;
        case CATMULLCLARK:
            m_subdivData[m_iter] = std::make_tuple( new Ra::Core::Geometry::CatmullClarkSubdivider(),
                                                    TopologicalMesh( mesh ), TriangleMesh() );
            break;
        }
        auto& [subdiv, topo, triMesh] = m_subdivData[m_iter];
        subdiv->attach( topo );
        ( *subdiv )( m_iter );
        triMesh = topo.toTriangleMesh();
    } else
    {
        switch ( m_subdivMethod )
        {
        case LOOP:
        {
            auto& [subdiv, topo, triMesh] = m_subdivData[m_iter];
            static_cast<Ra::Core::Geometry::LoopSubdivider*>( subdiv )
                ->recompute( mesh.vertices(), mesh.normals(),
                             triMesh.verticesWithLock(),
                             triMesh.normalsWithLock(),
                             topo );
            triMesh.verticesUnlock();
            triMesh.normalsUnlock();
            break;
        }
        case CATMULLCLARK:
        {
            auto& [subdiv, topo, triMesh] = m_subdivData[m_iter];
            static_cast<Ra::Core::Geometry::CatmullClarkSubdivider*>( subdiv )
                 ->recompute( mesh.vertices(), mesh.normals(),
                              triMesh.verticesWithLock(),
                              triMesh.normalsWithLock(),
                              topo );
            triMesh.verticesUnlock();
            triMesh.normalsUnlock();
            break;
        }
        }
    }
    Ra::Core::Geometry::TriangleMesh sMesh = std::get<2>( m_subdivData[m_iter] );
    m_mesh->loadGeometry( std::move( sMesh ) );
}

} // namespace PostSubdivPlugin
